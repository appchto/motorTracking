# Node.js Motor Bike Tracking

This an app that offers motor bike services. 
this app uses Node.js, Express, Passport, Mongoose, EJS and some other packages.

### Version: 1.0.1

### Usage

```sh
npm install

npm start

# Or run with Nodemon
npm run dev

# Visit http://localhost:5001
http://localhost:5001/map2 

```

### Map Tracker  

```sh

* user open map and will show his location
* when user moves will mark his location on the map with a marker
* map checks every 40 secs for movements or diff in location
* coords will be saved in the database
* 

* prod link

https://motortracking.herokuapp.com/map2

* test

* npm test .\test\motortracking.spec.js

```

### Routes  
```sh


```


